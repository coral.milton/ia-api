from flask import Flask, request, jsonify
import spacy
import re
import pickle
import psycopg2
from psycopg2 import sql

application = Flask(__name__)


with open('ModeloIA.pkl', 'rb') as file:
    clf = pickle.load(file)

with open('tfidf.pkl', 'rb') as file:
    vectorizer = pickle.load(file)    

nlp = spacy.load('es_core_news_sm')

db_params = {
    'host': '3.222.146.236',
    'database': 'prueba',
    'user': 'postgres',
    'password': 'miltonxd123',
}

conn = psycopg2.connect(**db_params)
cur = conn.cursor()


def extract_keywords(text):
    doc = nlp(text)
    keywords = [token.lemma_ for token in doc]
    return " ".join(keywords)

@application.route('/obtener_prediccion', methods=['POST'])
def predict_iva():
    data = request.get_json()
    nombre_producto = data.get('nombre_producto', '')
    detalles= data.get('detalles', '')
    id_producto=data.get('id_producto', '')
    text=nombre_producto+" "+detalles
    


    cleaned_text = re.sub(r'[^a-zA-Z\sáéíóúÁÉÍÓÚüÜñÑ]', '', text.lower())
    keywords = extract_keywords(cleaned_text)

    # Assuming you have a 'vectorizer' variable defined somewhere
    prueba = vectorizer.transform([keywords])

    prediction = clf.predict(prueba)

    update_query = sql.SQL("UPDATE prueba.inventario_productos SET prediccion={} WHERE id_producto={};").format(
    sql.Literal(int(prediction[0])),
    sql.Literal(int(id_producto))
    )
    
    cur.execute(update_query)
    conn.commit()
    return ""

@application.route('/getIva', methods=['POST'])
def predict():
    data = request.get_json()
    nombre_producto = data.get('nombre_producto', '')
    detalles= data.get('detalles', '')
    text=nombre_producto+" "+detalles
    


    cleaned_text = re.sub(r'[^a-zA-Z\sáéíóúÁÉÍÓÚüÜñÑ]', '', text.lower())
    keywords = extract_keywords(cleaned_text)

    # Assuming you have a 'vectorizer' variable defined somewhere
    prueba = vectorizer.transform([keywords])

    prediction = clf.predict(prueba)

    return jsonify({"prediction": prediction.tolist()})


@application.route('/')
def inicial():
    return "IA API"
